import time
import os
import argparse
import requests
import json
from tqdm import tqdm
from transformers import BertTokenizer, BertForNextSentencePrediction
import torch
from sentence_transformers import SentenceTransformer, util
from elasticsearch import Elasticsearch, helpers
from transformers import logging
import en_core_web_sm
from dataclasses import dataclass
import sys
import xml.etree.ElementTree as ET
import pandas as pd
import numpy as np

logging.set_verbosity_error()
INDEX = "porthos_premises_and_conclusions"

print("Load Models...")
# load sentence matching models
bert_tokenizer = BertTokenizer.from_pretrained("bert-base-uncased")
model_nsp = BertForNextSentencePrediction.from_pretrained("bert-base-uncased")
model_sentsim = SentenceTransformer("all-MiniLM-L6-v2")
print("Models loaded...")


@dataclass
class Document:
    id: str
    sentence: str
    score: float


@dataclass
class SentencePair:
    sent_first: Document
    sent_second: Document
    # score: float


def search(query: str, size: int = 10) -> list[Document]:
    response = es.search(index=INDEX, body=query, size=size)
    return [
        Document(
            id=hit["_source"]["id"],
            sentence=hit["_source"]["sentence"],
            score=hit["_score"],
        )
        for hit in response["hits"]["hits"]
    ]


def get_similarity_nsp_bert(
    query_results: list[dict],
    top_x: int,
    model: BertForNextSentencePrediction,
    tokenizer: BertTokenizer,
) -> list[tuple[str, str]]:

    top_x_results = query_results[:top_x]
    matched_results = {}
    used_sentences_index = set()
    score = 0
    for i, doc_first in enumerate(top_x_results):
        best_match = (0, 0)
        for j in range(len(top_x_results), len(query_results)):
            if j in used_sentences_index:
                continue
            doc_second = query_results[j]
            try:
                encoding = tokenizer(
                    doc_first.sentence,
                    doc_second.sentence,
                    return_tensors="pt",
                )
                outputs = model(**encoding, labels=torch.LongTensor([1]))
                score = outputs[1].detach().numpy()[0][0]
            except:
                # print(f"ENCODING PROBLEMS WITH: {doc_first}, {doc_second}")
                pass
            if score > best_match[1]:
                best_match = (j, score)
        used_sentences_index.add(best_match[0])
        matched_results[i] = best_match[0]
    return [
        (top_x_results[i], query_results[matched_results[i]]) for i in matched_results
    ]


def get_similarity_sentence_similarity_bert(
    query_results: list[str], top_x: int, model: SentenceTransformer
) -> list[tuple[str, str]]:

    top_x_results = query_results[:top_x]
    comparing_results = query_results[top_x:]
    top_x_results_embedded = model.encode(top_x_results, convert_to_tensor=True)
    comparing_results_embedded = model.encode(comparing_results, convert_to_tensor=True)
    cosine_scores = util.pytorch_cos_sim(
        top_x_results_embedded, comparing_results_embedded
    )
    matched_results = {}
    for i, v in enumerate(cosine_scores):
        similarities = []
        len_top_result = len(v)
        for j in range(i + 1, len_top_result):
            similarities.append((j, cosine_scores[i][j]))
        sorted_similarities = sorted(
            similarities, key=lambda x: x[1].item(), reverse=True
        )
        matched_results[i] = sorted_similarities
    return [
        (top_x_results[i], comparing_results[matched_results[i][0][0]])
        for i in matched_results
    ]


def get_similarity(
    query_results: list[Document],
    top_x: int,
    sim_mode: str,
    tokenizer: BertTokenizer = bert_tokenizer,
    model_nsp: BertForNextSentencePrediction = model_nsp,
    model_sentsim: SentenceTransformer = model_sentsim,
) -> list[tuple[str, str]]:
    if sim_mode == "nsp":
        similarities = get_similarity_nsp_bert(
            query_results, top_x, model_nsp, bert_tokenizer
        )
    if sim_mode == "sent_sim":
        similarities = get_similarity_sentence_similarity_bert(
            query_results, top_x, model_sentsim
        )
    return similarities


def classify_results(topic: str, results: list[Document], classifier):
    argumentative_results = []
    non_argumentative_results = []
    for result in results:
        classified_sent = classifier.classify(topic, result.sentence)
        if classified_sent.predicted_label != "NoArgument":
            argumentative_results.append(result)
        else:
            non_argumentative_results.append(result)
    return argumentative_results, non_argumentative_results


def build_query(query_term: str):
    query = {
        "query": {
            "bool": {
                "should": [
                    {"match": {"sentence": {"query": query_term, "operator": "and"}}},
                    {"match": {"sentence": {"query": query_term, "operator": "or"}}},
                    {"match": {"conclusion": {"query": query_term, "operator": "or"}}},
                ]
            }
        }
    }
    return query


def add_noun_chunk_booster_to_query(topic: str, query: dict, nlp) -> dict:

    doc = nlp(topic)
    print(topic)
    match_query = {"match": {"sentence": {"operator": "AND"}}}
    matchphrase_sentence_query = {"match_phrase": {"sentence": {"boost": 2}}}
    matchphrase_conclusion_query = {"match_phrase": {"conclusion": {"boost": 2}}}

    for i, chunk in enumerate(doc.noun_chunks):
        chunk = str(chunk)

        if len(chunk) < 3:
            continue
        match_query["match"]["sentence"]["query"] = chunk
        query["query"]["bool"]["should"].append(match_query)
        matchphrase_sentence_query["match_phrase"]["sentence"]["query"] = chunk
        query["query"]["bool"]["should"].append(matchphrase_sentence_query)
        matchphrase_conclusion_query["match_phrase"]["conclusion"]["query"] = chunk
        query["query"]["bool"]["should"].append(matchphrase_conclusion_query)
        match_query = {"match": {"sentence": {"operator": "AND"}}}
        matchphrase_sentence_query = {"match_phrase": {"sentence": {"boost": 2}}}
        matchphrase_conclusion_query = {"match_phrase": {"conclusion": {"boost": 2}}}
    return query


def query_for_best_match(
    query_string: str,
    nlp,
    sim_mode: str = "nsp",
    result_size: int = 10,
    top_x: int = 5,
    to_be_classified: bool = False,
    classifier=None,
) -> list[SentencePair]:
    """Query for results.

    Parameters:
    query_string -- the query string to retrieve the arguments
    sim_mode -- the similarity mode the results are combined by. "nsp" for similarity by NextSentencePrediction, "sent_sim" for similarity by BertSentenceSimilarity
    """

    query = add_noun_chunk_booster_to_query(
        query_string, build_query(query_string), nlp
    )
    query_results = search(query, size=result_size)
    if to_be_classified:
        argumentative_results, non_argumentative_results = classify_results(
            query_string, query_results, classifier=classifier
        )
        query_results = argumentative_results + non_argumentative_results
    sentence_tuple = get_similarity(
        query_results=query_results, top_x=top_x, sim_mode=sim_mode
    )

    return [
        SentencePair(sent_first=pair[0], sent_second=pair[1]) for pair in sentence_tuple
    ]


if __name__ == "__main__":

    # PARSE ARGUMENTS
    parser = argparse.ArgumentParser()
    parser.add_argument("-o", "--output-dir")
    parser.add_argument("-t", "--tag")
    args = parser.parse_args()
    args = vars(args)

    # CONSTANTS
    HOST_NAME = "advanced-information-retrieval-porthos-elastic-1"

    # HANDLE INPUT
    output_dir = args["output_dir"]
    tag = args["tag"]

    # MAKE SURE THE OUTPUT DIRECTORY EXISTS
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
        print(f"Creating {output_dir}...\n")
    else:
        print(f"$output_dir: {output_dir} already exists...\n")
    sys.path.insert(
        0,
        "/root/util/acl2019-BERT-argument-classification-and-clustering/argument-classification",
    )
    import inference

    es = Elasticsearch(hosts=HOST_NAME, timeout=300, retry_on_timeout=True)
    time.sleep(60)
    es.cluster.health(wait_for_status="yellow", request_timeout=1)
    print("Elasticsearch connection established")
    ## load acl-bert model
    classifier = inference.BertArgumentClassifier(
        model_path="/root/util/acl2019-BERT-argument-classification-and-clustering/argument-classification/bert_output/argument_classification_ukp_all_data"
    )

    # load nlp
    nlp = en_core_web_sm.load()

    tree = ET.parse("topics.xml")
    root = tree.getroot()
    topics = [
        {"topic_id": child[0].text, "topic_sent": child[1].text} for child in root
    ]

    for topic in tqdm(topics):
        print(topic)
        results = query_for_best_match(
            classifier=classifier,
            nlp=nlp,
            query_string=topic["topic_sent"],
            sim_mode="nsp",
            result_size=100,
            top_x=10,
            to_be_classified=True,
        )
        for rank, pair in enumerate(results):
            ranked_pairs = f'{topic["topic_id"]} Q0 {pair.sent_first.id},{pair.sent_second.id} {rank+1} {pair.sent_first.score} {tag}'
            # APPEND CURRENT DATAFRAME TO OUTPUT FILE
            with open(f"{output_dir}/run.txt", "a+") as run_file:
                run_file.write(ranked_pairs + "\n")

            # LOOK AT WHAT WAS ACTUALLY WRITTEN TO FILE
            print(ranked_pairs)
