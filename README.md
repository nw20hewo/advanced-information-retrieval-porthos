# Advanced Information Retrieval Porthos

## Usage

Install docker

Execute the notebooks in their enumerated order with the args.me corpus: args_processed.csv to process the data parquet file with our preprocessing pipeline.
Alternatively, the preprocessing of the data by our pipeline can be skipped by downloading and using the following file:
https://drive.google.com/file/d/1f84gRq5M1KrbwInPOf3H4KrGUHx97-K5/view?usp=sharing

Create a directory /data at the root of repository and move the data file into it.

Execute the following command to create the docker container for the python and elastic environment.

```shell script
docker-compose up -d
```

The indexing is done via this command wiht the -d flag for the chosen data file:

```shell script
docker exec -it advanced-information-retrieval-porthos-python-1 python index.py --data [path_to_data_file]
```

For the ACL-Bert Classifier:
Download and unzip the pre-trained model from here:
- [argument_classification_ukp_all_data.zip](https://public.ukp.informatik.tu-darmstadt.de/reimers/2019_acl-BERT-argument-classification-and-clustering/models/argument_classification_ukp_all_data.zip) and move it to 'util/acl2019-BERT-argument-classification-and-clustering/argument-classification/bert_output/'

The retrieval is done by this command with the flag -o to specify the output directory and the flag -t to name the run:



```shell script
docker exec -it advanced-information-retrieval-porthos-python-1 python run.py -o output -t [tag]
```

## FAQ

If indexing stops -> increase docker ram ~ 8gb.

## Links

[Course Link](https://temir.org/teaching/information-retrieval-ws21/information-retrieval-ws21.html)

[Cloud](https://ecloud.global/s/eWrepARxJR2yZrN)

[Zwischenpräsentation](https://docs.google.com/presentation/d/1UyW3BjMTZ29mdxXV4XfE8oxEXGSq9xOTxX16rxmmTmg/edit#slide=id.g1074352843f_0_0)

[Abschlusspräsentation](https://docs.google.com/presentation/d/1qf5ueYJ1F-1MqznSB8G5XQ9LFMsuOVm_aO3Ooiu0Jl4/edit?usp=sharing)

[Lab Report (Overleaf)](https://www.overleaf.com/5831385739wpzxswrsgjbm)

[Paper Template + Instructions](https://www.overleaf.com/read/bsppqnwcmxbr)

## Retrieval Model Feature Set

### Pre-Processing

- ACL-Bert Classification on dataset [0/1]
- Argument Classifier: SVM WebisArgsQuality2020 [0/1]
- Sentence Classification: Sentence contains verb [0/1]

### Index
- Data: [Premise/_Premise+Conclusion_]
- Ranking Model: [BM25/DirichletLM mu=2000/DirichletLM mu=116]

### Retrieval
- Query: [Match Sentence No Boosting/+Match Sentence boost by corresponding Conclusion/+_boost specific phrases_]
- ACL-Bert Classification on retrieved sentences query based [0/1]
- Sentence Pairing: [SBert(Sentence Similarity)/Next Sentence Prediction/_Neighbor Matching_/_Random Matching_]

|                                                                                                                                                                                                       | ACL-Classifier | SVM-Classifier | Sentence-Classifier | Data    | Ranking   | Filter | Query                | ACL-Classifier | Sentence Pairing | Precision | nDCG@K |
|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------|----------------|---------------------|---------|-----------|--------|----------------------|----------------|------------------|-----------|--------|
| [BM25 + Sentence Similarity](https://docs.google.com/spreadsheets/d/1yarPbMEn1nHJkW3RTR4uUR5dqIoXVqD7soceIZy4d0E/edit#gid=2116189548)                                                                 | 0              | 0              | 0                   | Premise | BM25      | 0      | Simple               | 0              | SentSim          | 0.34      | 0.55   |
| [ BM25 + Next Sentence Prediction ](https://docs.google.com/spreadsheets/d/182pmWB_pUO5Bnvz5X-3xo-5zOd-zrYHj3O7QBm3zAHY/edit#gid=175605494)                                                           | 0              | 0              | 0                   | Premise | BM25      | 0      | Simple               | 0              | NSP              | 0.47      | 0.65   |
| [ BM25 + Next Sentence Prediction + Query Based Filtering ](https://docs.google.com/spreadsheets/d/1CsW-a5Gf14eeo0ni6iNQncvKdwWoG4i9optmK0m5DcI/edit?usp=sharing)                                     | 0              | 0              | 0                   | Premise | Bm25      | 0      | Simple               | 1              | NSP              | 0.54      | 0.685  |
| [ DirichletLM_Mu_116 + Next Sentence Prediction ](https://docs.google.com/spreadsheets/d/1kx3yKUn39iVfekL3MKMg32N5blTLn62JBBhCsUXmTXM/edit?usp=sharing)                                               | 0              | 0              | 1                   | Premise | Dirichlet | 0      | Simple               | 0              | NSP              | 0.36      | 0.62   |
| [DirichletLM_Mu_116 + Next Sentence Prediction + Bool Query with extra conclusion field ](https://docs.google.com/spreadsheets/d/1tN0IK2g2rST_Wq9bp7k8SZbh_WnLfJ9NGpR5-zP7-EE/edit?usp=sharing)       | 0              | 0              | 1                   | Premise | Dirichlet | 1      | Boolean + Conclusion | 0              | NSP              | 0.48      | 0.59   |
| [DirichletLM_Mu_116 + Next Sentence Prediction + SVM ](https://docs.google.com/spreadsheets/d/1wEalsHXD4-EsdMe049TFE2DF0CnHjt2eU4694wt0ml8/edit?usp=sharing)                                          | 0              | 1              | 1                   | Premise | Dirichlet | 0      | Simple               | 0              | NSP              | 0.36      | 0.62   |
| [ACL-Preprocessed + DirichletLM_Mu_116 + Next Sentence Prediction ](https://docs.google.com/spreadsheets/d/1ud8H1z2-f7x4khX9em2H8yqYz7jGBBcuqC-aqr5Y_Ac/edit?usp=sharing)                             | 1              | 0              | 1                   | Premise | Dirichlet | 0      | Simple               | 0              | NSP              | 0.39      | 0.569  |
| [ACL-Preprocessed + DirichletLM_Mu_116 + Next Sentence Prediction + ACL Query Based Filtering ](https://docs.google.com/spreadsheets/d/1VICX4K2NbPqxMyD29lrAwrH3xnY5YZR80ZgCsMvmyko/edit?usp=sharing) | 1              | 0              | 1                   | Premise | Dirichlet | 0      | Simple               | 1              | NSP              | 0.42      | 0.617  |
| [Premise&Conclusion + DirichletLM_Mu_116 + Filter + Noun Chunks + Next Sentence Prediction ](https://docs.google.com/spreadsheets/d/1whuoxceKIcMJeCZAeYnkl9EakRq1SA53NSzWujwRmR8/edit#gid=835325880) | 0              | 0              | 1                   | Premise&Conclusion | Dirichlet | 1      | Boolean + Noun Chunking               | 0              | NSP              | 0.54      | 0.84  |
| [Premise&Conclusion + DirichletLM_Mu_116 + Filter + Noun Chunks + Next Sentence Prediction + ACL ](https://docs.google.com/spreadsheets/d/1DiN6Qm5gi_KrPaNQMhbdNU85Gimg9vbHcNYVqEMhEP8/edit#gid=1942991003) | 0              | 0              | 1                   | Premise&Conclusion | Dirichlet | 1      | Boolean + Noun Chunking              | 1              | NSP              |    0.67   | 0.73  |


## Milestones

- [x] Literature Research [~ 2 weeks]
- Find existing research relevant to the task.
- [x] Data Analysis [~ 2 weeks]
- Take a closer look at the data, use descriptive statistics, identify interesting patterns.
- [x] Technology Stack [~ 2 weeks]
- Decide upon the software libraries you are going to use.
- [x] Vertical Prototype [~ 2 weeks]
- Build a working prototype with a basic retrieval model.
- [x] Refined Prototype [~ 2 weeks]
- Build a prototype that uses an advanced/refined retrieval model.
- [x] Deployment on Tira [~ 2 weeks]
- Build a containerized version of your software and deploy it on the Tira platform.
- [x] Evaluation [~ 3 weeks]
- Evaluate the results of your retrieval models.
- [x] Documentation [~ 2 weeks]
- Write a README, including deployment instructions.
- [x] Report [~ 3 weeks]
- Write the final report.

## Literature

### Overview

- Bondarenko et al. Overview of Touché 2021: Argument Retrieval. (CLEF 2021). [link](https://webis.de/downloads/publications/papers/bondarenko_2021c.pdf)

### Task 1

- Wachsmuth et al. Building an Argument Search Engine for the Web (ArgMining 2017). [link](https://webis.de/downloads/publications/papers/wachsmuth_2017f.pdf)

- Ajjour et al. Data Acquisition for Argument Search: The args.me corpus. (KI 2019). [link](https://webis.de/downloads/publications/papers/ajjour_2019a.pdf)
- Potthast et al. Argument Search: Assessing Argument Relevance. (SIGIR 2019). [link](https://webis.de/downloads/publications/papers/potthast_2019k.pdf)
- Wachsmuth et al. Computational Argumentation Quality Assessment in Natural Language. (EACL 2017). [link](https://webis.de/downloads/publications/papers/wachsmuth_2017b.pdf)

### Additional Literature

- Learning to Rank Arguments with Feature Selection. [link](http://ceur-ws.org/Vol-2936/paper-207.pdf)

- Augmented SBERT: Data Augmentation Method for Improving (Bi-Encoders for Pairwise Sentence Scoring Tasks. [link](https://arxiv.org/pdf/2010.08240v2.pdf)

## Links

### Libaries

- <https://github.com/terrier-org/pyterrier>
- <https://www.nltk.org/>
- [BERT argument classification (pro, con, not an argument)](https://github.com/UKPLab/acl2019-BERT-argument-classification-and-clustering)

### Misc

- https://medium.com/analytics-vidhya/sentence-extraction-using-textrank-algorithm-7f5c8fd568cd
- https://towardsdatascience.com/all-the-pandas-read-csv-you-should-know-to-speed-up-your-data-analysis-1e16fe1039f3
- https://towardsdatascience.com/create-a-simple-search-engine-using-python-412587619ff5
- http://ethen8181.github.io/machine-learning/search/bm25_intro.html
- https://www.elastic.co/de/blog/practical-bm25-part-2-the-bm25-algorithm-and-its-variables
- Query Expansion: https://medium.com/@swaroopshyam0/a-simple-query-expansion-49aef3442416
- https://qa.fastforwardlabs.com/elasticsearch/qa%20system%20design/passage%20ranking/masked%20language%20model/word%20embeddings/2020/07/22/Improving_the_Retriever_on_Natural_Questions.html

## Offene Fragen

- Gibt es einen Datensatz der Argumente mit Rhetorical, Logical und Dialectical Bewertungen versieht?
  - [Ja](https://webis.de/data/webis-argument-quality-20.html)
- Wie erfolgt das Preprocessing?
- Premise oder Conclusion querien?

## Roadmap

- [ ] Ist ein Satz ein Argument? - István
  - [ ] Mögliche Klassifizierung durch [webis-argument-quality data](https://webis.de/data/webis-argument-quality-20.html) und support vector machines
  - [ ] Mögliche Klassifizierung durch [BERT argument classification](https://github.com/UKPLab/acl2019-BERT-argument-classification-and-clustering)
    - -> https://github.com/istvan-derda/acl2019-BERT-argument-classification-and-clustering/
- [ ] Relevanz bestimmen - Pia
  - [ ] Mögliche Implementierung durch DirichletLM
- [ ] Satzpaar Score bestimmen - Nils
  - [ ] Mögliche Implementierung durch Augmented SBERT
- [ ] Fasst das Satzpaar das Argument zusammen?

## Output format

Runs may be either automatic or manual. An automatic run must not "manipulate" the topic titles via manual intervention. A manual run is anything that is not an automatic run. Upon submission, please let us know which of your runs are manual. For each topic, include up to 1,000 retrieved sentence pairs. Each team can submit **up to 5** different runs.

​ The submission format for the task will follow the standard TREC format:

​ `qid stance pair rank score tag`

​ With:

- `qid`: The topic number.
- `stance`: The stance of the sentence pair ("PRO" or "CON").
- `pair`: The pair of sentence IDs (from the provided version of the args.me corpus) returned by your system for the topic `qid`.
- `rank`: The rank the document is retrieved at.
- `score`: The score (integer or floating point) that generated the ranking. The score must be in descending (non-increasing) order: it is important to handle tie scores.
- `tag`: A tag that identifies your group and the method you used to produce the run.

​ The fields should be separated by a whitespace. The individual columns' widths are not restricted (i.e., score can be an arbitrary precision that has no ties) but it is important to include all columns and to separate them with a whitespace.

​ An example run for Task 1 is:

`1 PRO Sf9294c83-Af186e851_sent1,Sf9294c83-A9a4e056e_sent3 1 17.89 myGroupMyMethod`
`1 PRO Sf9294c83-A9a4e056e_sent2,Sf9294c83-A9a4e056e_sent4 2 16.43 myGroupMyMethod`
`1 CON S96f2396e-Aaf079b43_sent1,Sf9294c83-Af186e851_sent5 3 16.42 myGroupMyMethod`
`...`
**Note:** If you do not have stance information, use `Q0` as the value in the stance column.

## Evaluation / Goal

From [webis.de](https://webis.de/events/touche-22/shared-task-1.html):

> Be sure to retrieve a pair of ''strong'' argumentative sentences. Our human assessors will label the retrieved pairs manually, both for their general topical relevance and for their argument quality, i.e.,: (1) whether each sentence in the retrieved pair is argumentative (claim, supporting premise, or conclusion), (2) whether the sentence pair forms a coherent text (sentences in a pair must not contradict each other), (3) whether the sentence pair forms a short summary of the corresponding arguments from which these sentences come from; each sentence in the pair must ideally be the most representative / most important sentence of its corresponding argument.
> The format of the relevance/quality judgment file:
>
> `qid 0 pair rel`
>
> With:
>
> `qid`: The topic number.
>
> `0`: Unused, always 0.
>
> `pair`: The pair of sentence IDs (from the provided version of the args.me corpus).
>
> `rel`: The relevance judgment: -2 non-argument (spam), 0 (not relevant) to 3 (highly relevant). The quality judgment: 0 (low) to 3 (high).

## Fragen für Konsultation am 30.11.21?

- Wie viel Ergebnisse sollen wir mindestens zurückgeben (up to 1000)? -> ask @Shabaz Syed
- Soll es gleichviele Pro- und Contra-Argumente zu einer Query ausgegeben werden? (wäre z.B 10 Pro-Argumente und 0 Contra-Argumente ok?)
- Laufzeit ist nicht entscheident oder? - Gibt es ein Timeout?
- Lässt sich Precision und Recall als Evaluationmaß überhaupt verwenden? (true positives sind nicht bekannt)
- Sollen wir für die Evaluation nDCG@k nehmen? oder reicht es beispielsweise die Relevanz der einzelnen Satzpaare nach dem Schema (0 = low bis 3 = high, -2 = spam) zu bewerten und einen Durchschnitt zu berechnen?
- Ist DirichletLMxBERT ein guter Name für unseren Talk?

## Zwischenpräsentation: Garbage in - Arguments out: Using BERT for Argument retrieval

- Welchen Task Sie bearbeiten (muss nicht weiter erklärt werden)
  Task 1
- Technology Stack (welche Technologien haben Sie zur Umsetzung verwendet)
  - Elasticsearch
  - Jupyter Notebook?
  - Python (Pandas, Parquet-Files, NLTK)
  - transformers (sentence_transformer: SBert, BertForNextSentencePrediction)
- Vertikaler Prototyp (welche Daten benutzen Sie für die Indizierung, welches Retrievalmodell verwenden Sie)
  - args_processed Datensatz -> ausgepackt in Sätze
  - pre-processing + Entfernung von Duplikaten (Von ~5_700_000 auf ~5_000_000 Sätze)
  - DirichletLM + Elasticsearch
- Refined Prototyp (Idee des eigenen Ansatzes, ggf. Related Work, ggf. Umsetzung)
  - Ergebnisse (Retrievte Top Sätze) des Vertikalen Prototypen sind Quatsch -> Abfrage auf "ist es ein Argument?"
  - Erster Ansatz: Support Vector Machines analog zu Touch 2021 Heimdall - Gienapp, L. (2021). Quality-aware argument retrieval with topical clustering. CEUR Workshop Proceedings, 2936, 2366–2373.
    - Liefert jedoch keine zufriedenstellenden Ergebnisse. Annahme: SVM wurde auf Webis-ArgQuality-20 Datensatz traininert. Dieser umfasst mehrere Sätze. Wir arbeiten auf einzelnen Sätzen -> Klassifizierung funktioniert nicht.
    - Zweiter Ansatz: Argument Klassifikation mittels Bert: Reimers, N., Schiller, B., Beck, T., Daxenberger, J., Stab, C., & Gurevych, I. (2020). Classification and clustering of arguments with contextualized word embeddings. ACL 2019 - 57th Annual Meeting of the Association for Computational Linguistics, Proceedings of the Conference, 567–578. https://doi.org/10.18653/v1/p19-1054  
  - Sentence Pairing: Zwei Ansätze
    - Sentence Similarity: Intuition erklären [related Literatur hinzufügen]
    - Next Sentence Prediction: [related Literatur hinzufügen]
- Ergebnisse aus Evaluation (welches Evaluationsmaß, wie haben Sie es berechnet, welche Ergebnisse haben Sie erhalten, ggf. Vergleich der Ergebnisse zwischen vertikalem und refined Prototyp)
  - Ausstehend
  - Precision
  - Eigene Evaluation
  - Einsatz von nDCG@k

## Todo 10. Januar

- [ ] weitere Evaluation + Evaluationsmetrik für Sentence Pairing bestimmen
- [ ] Ausblick aus Zwischenpräsentation umsetzen
  - [ ] Query Expansion
  - [ ] Query Embedding
  - [ ] Argument Classification zur Laufzeit auf retrievte Argumente
- [ ] Tira Auseinandersetzung
- [ ] Vortragsgleiderung: Deadline 17. Januar

## Fragen für Konsultation am 11.01.22?

- Soll die Präsentation von allen gehalten werden?

### Feedback zur Zwischenpräsentation

- Wir waren etwas zu "wortkarg"
- Nur zu sagen, "der ndcg ist xy" reicht nicht
- Flowcharts kommen gut an
- Folien sollten selbsterklärend sein

### Weiterarbeit

- Die Abschlusspräsentation sollte zeigen, dass seit der Zwischenpräsi noch was passiert ist
- Bei Tira lassen sich 5 runs einreichen -> lohnt sich das zu nutzen
- System so bauen, dass Komponenten an- und ausknipsbar sind
- Im Paper verwerten: durch query-expansion steigt der Recall (zitieren und von Erfahrung berichten -> reproduktion eines ergebnisses)
- Argument-Classification mit Bert vielversprechend
- noch einen zweiten vielversprechenden Ansatz versuchen! -> z.b query embedding

## Treffen 11.01.22

- Idee: Schrottparagraphen im preprocessing rausschmeißen mit
- Lass Aussagekräftigere Folien machen
- 25.2. Vortrag!
- Im Vortrag herausstellen, dass wir bewusst auf einzelne Sätze gehen.
- Frage: Zu wann müssen die Tira-Runs abgegeben sein? Was darf man zwischen abschlusspräsi und paperabgabe noch machen?
