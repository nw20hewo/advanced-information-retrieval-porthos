from elasticsearch import Elasticsearch, helpers
from tqdm import tqdm
import pandas as pd
import argparse


def index_batch(docs):
    requests = []
    for i, doc in enumerate(docs):
        request = doc
        request["_op_type"] = "index"
        request["_index"] = INDEX_NAME
        requests.append(request)
    helpers.bulk(es, requests)


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--data", type=str, required=True)
    args = parser.parse_args()

    print("Load Data...")

    df = pd.read_parquet(args.data)

    print("Data loaded.\nEstablish connection to elasticsearch")

    es = Elasticsearch(
        hosts="advanced-information-retrieval-porthos-elastic-1", timeout=300
    )

    print("Connection established")

    INDEX_NAME = "porthos_premises_and_conclusions"
    BATCH_SIZE = 1000

    mu = round(df["sentence"].apply(len).mean())
    body = {
        "settings": {
            "index": {
                "similarity": {"dirichletlm": {"type": "LMDirichlet", "mu": mu}},
                "analysis": {
                    "filter": {
                        "english_stop": {"type": "stop", "stopwords": "_english_"},
                        "my_stemmer": {"type": "stemmer", "name": "english"},
                    },
                    "analyzer": {
                        "porthos_analyzer": {
                            "type": "custom",
                            "tokenizer": "standard",
                            "filter": ["lowercase", "english_stop", "my_stemmer"],
                        }
                    },
                },
            },
            "number_of_shards": 1,
        },
        "mappings": {
            "properties": {
                "sentence": {
                    "similarity": "dirichletlm",
                    "analyzer": "porthos_analyzer",
                    "type": "text",
                },
                "conclusion": {
                    "similarity": "dirichletlm",
                    "analyzer": "porthos_analyzer",
                    "type": "text",
                },
            }
        },
    }
    if es.indices.exists(index=INDEX_NAME):
        print(f"\nDELETE INDEX: {INDEX_NAME}\n")
        es.indices.delete(index=INDEX_NAME, ignore=[404])
    print(f"Create Index: {INDEX_NAME}\n")
    es.indices.create(index=INDEX_NAME, body=body)
    docs = []
    count = 0
    print("\nNow indexing...\n")
    for doc in tqdm(df.iterrows()):
        doc = doc[1].to_dict()
        docs.append(doc)
        count += 1

        if count % BATCH_SIZE == 0:
            index_batch(docs)
            docs = []

    if docs:
        index_batch(docs)

    es.indices.refresh(index=INDEX_NAME)
    print("\nDone indexing.\n")
