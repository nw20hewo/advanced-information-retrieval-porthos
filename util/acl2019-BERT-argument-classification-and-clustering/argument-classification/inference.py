"""
Runs a pre-trained BERT model for argument classification.

You can download pre-trained models here: https://public.ukp.informatik.tu-darmstadt.de/reimers/2019_acl-BERT-argument-classification-and-clustering/models/argument_classification_ukp_all_data.zip

The model 'bert_output/ukp/bert-base-topic-sentence/all_ukp_data/' was trained on all eight topics (abortion, cloning, death penalty, gun control, marijuana legalization, minimum wage, nuclear energy, school uniforms) from the Stab et al. corpus  (UKP Sentential Argument
Mining Corpus)

Usage: python inference.py

"""

from dataclasses import dataclass
from pprint import pprint
from typing import List

import numpy as np
import torch
from pytorch_pretrained_bert.modeling import BertForSequenceClassification
from pytorch_pretrained_bert.tokenization import BertTokenizer
from torch.utils.data import TensorDataset, DataLoader, SequentialSampler

from train import InputExample, convert_examples_to_features

num_labels = 3
default_model_path = "bert_output/argument_classification_ukp_all_data/"
label_list = ["NoArgument", "Argument_against", "Argument_for"]
max_seq_length = 64
eval_batch_size = 8

# Input examples. The model 'bert_output/ukp/bert-base-topic-sentence/all_topics/' expects text_a to be the topic
# and text_b to be the sentence. label is an optional value, only used when we print the output in this script.


@dataclass()
class ArgumentClassificationResult:
    topic: str
    sentence: str
    predicted_label: str

    def __repr__(self):
        return (
            f"ArgumentClassificationResult(\n"
            f"|   topic: {self.topic}\n"
            f'|   "{self.sentence}"\n'
            f"|   predicted label: {self.predicted_label})"
        )


@dataclass()
class ArgumentClassificationInput:
    topic: str
    sentence: str


class BertArgumentClassifier:
    def __init__(self, model_path=default_model_path):
        self.tokenizer = BertTokenizer.from_pretrained(model_path, do_lower_case=True)
        self.model = BertForSequenceClassification.from_pretrained(
            model_path, num_labels=num_labels
        )
        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        self.model.to(self.device)
        self.model.eval()

    def _inference(
        self, sentence_pairs: List[ArgumentClassificationInput]
    ) -> List[ArgumentClassificationResult]:

        # conversion so I don't have to touch internal code
        internal_argument_input_repr = [
            InputExample(
                text_a=sentence_pair.topic,
                text_b=sentence_pair.sentence,
                label="NoArgument",
            )
            for sentence_pair in sentence_pairs
        ]
        eval_features = convert_examples_to_features(
            internal_argument_input_repr, label_list, max_seq_length, self.tokenizer
        )

        all_input_ids = torch.tensor(
            [f.input_ids for f in eval_features], dtype=torch.long
        )
        all_input_mask = torch.tensor(
            [f.input_mask for f in eval_features], dtype=torch.long
        )
        all_segment_ids = torch.tensor(
            [f.segment_ids for f in eval_features], dtype=torch.long
        )

        eval_data = TensorDataset(all_input_ids, all_input_mask, all_segment_ids)
        eval_sampler = SequentialSampler(eval_data)
        eval_dataloader = DataLoader(
            eval_data, sampler=eval_sampler, batch_size=eval_batch_size
        )

        predicted_labels = []
        with torch.no_grad():
            for input_ids, input_mask, segment_ids in eval_dataloader:
                input_ids = input_ids.to(self.device)
                input_mask = input_mask.to(self.device)
                segment_ids = segment_ids.to(self.device)

                logits = self.model(input_ids, segment_ids, input_mask)
                logits = logits.detach().cpu().numpy()

                for prediction in np.argmax(logits, axis=1):
                    predicted_labels.append(label_list[prediction])

        return [
            ArgumentClassificationResult(
                sentence_pair.topic, sentence_pair.sentence, predicted_label
            )
            for sentence_pair, predicted_label in zip(sentence_pairs, predicted_labels)
        ]

    def classify(self, topic: str, sentence: str) -> ArgumentClassificationResult:
        input_list = [ArgumentClassificationInput(topic, sentence)]
        result_list = self._inference(input_list)
        return result_list[0]

    def classify_batch(
        self, input_list: List[ArgumentClassificationInput]
    ) -> List[ArgumentClassificationResult]:
        return self._inference(input_list)


def main():
    classifier = BertArgumentClassifier()
    result = classifier.classify(
        topic="Do we need sex education in schools",
        sentence="I believe that education is necessary, but we do not need 'schools' for it.",
    )
    classification_inputs = [
        ArgumentClassificationInput(topic=example.text_a, sentence=example.text_b)
        for example in default_input_examples
    ]
    # result = classifier.classify_batch(classification_inputs)
    pprint(result)


if __name__ == "__main__":
    main()
